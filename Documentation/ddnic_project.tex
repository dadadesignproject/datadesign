\documentclass[12pt, a4paper]{article}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{pgfplots}
\usepackage{float}
\usepackage{color}

\title{Data Design and Nature Inspired Computing\\Genetic Algorithm applied to self-organizing amphiphilic systems}
\author{Matteo Carisi 848830\and Wandi Du 984210\and Giorgio Magnan 846314\and Ylenia Parin 848661\and Rossana Salaro 847168\and Giulia Sandi 865144\and Alberto Scalco 846175\and Andrea Sina 841723}

\begin{document}
\maketitle
\section{Introduction}
The experiment we developed is about the maximization of the turbidity function in a Self-Organizing Amphiphilic System. This system is composed by vesicles which are a basic tool of the cell for organizing metabolism, transport, enzyme storage, as well as being chemical reaction chamber.\\
The response, the turbidity function (T) is a measure of the number of evolving microstructures and their size. This property emerge from a chemical process, involving a large number of experimental variables, complex interaction between them and the influence of noise variables. Taking in account all these aspects, what we should do is to develop a huge number of experiments to find the well, which maximize the turbidity value. This would lead us to waste of time and resources.\\Thus, through a Genetic Algorithm (GA), we will simulate the evolution of a well population. This algorithm is based on a dynamic evolutionary technique, which aim is to improve the population characteristics through a changing process applied to the population's individuals. Our task is to select which are the discriminating techniques which will help the individuals with higher values of the fitness function to survive, at the expense of those that have lower values of fitness function which indeed have a negative contribution on the optimization. Using these techniques we will concentrate on the exploration rather than on the exploitation of available information. In this way, we do not consider local maximum which will lead us to wrong conclusions.\\

\section{Problem structure definition}
Within our problem, each composition is made by sixteen variables, among those there are: reactans, pH, ion strenght, temperature, electric field strength, flow rate, reaction time, etc..\\
Our X variable is defined as follow:\\
\begin{equation}
X = (X_1, ..., X_{16})
\end{equation}
where $X_i\in\{0, 0.2, 0.4, 0.6, 0.8, 1\} \quad\forall i = 1, ..., 16$. We need also to consider the following constraint:\\
\begin{equation}
\sum_{i=1}^{16} X_i = 1
\end{equation}
The experimental space we have to consider is $\Omega$  and it is the set of all possible combination of all the factors, their levels and their interactions. Its cardinality is exactly 15'504.\\
The function we have to maximize is the turbidity (T), so our response will be:\\
\begin{equation}
Y = T(X)
\end{equation}
\section{Methodology}
In order to simulate the evolution of a population, our algorithm consist in 20 iterations corresponding to 20 generations of individuals. Each generation is made by 10 individuals. The initial population will be randomly generated and the elements contained will respect the constraints previously defined.\\
Since our algorithm is a GA, it is made by selection, crossover and mutation operations. In particular at each iteration the following steps are repeated:
\begin{enumerate}
\item \textbf{Evaluation of the fitness function:} the first step consists in evaluating the fitness of the population in order to find which elements have higher fitness and, more important, the total fitness of the population;
\item \textbf{Selection:} the two best elements will pass directly to the mutation operation, without crossover; then, using the roulette wheel method, other 8 parents will be chosen and then we apply crossover on them;
\item \textbf{Crossover:} using the 8 parents extracted by the selection, the algorithm will generate 8 new children for the next generation;
\item \textbf{Mutation}: the resulting children (2 directly from the population and 8 generated by the crossover) will be mutated with a certain probability P.
\end{enumerate}
In the next chapter we will explain exactly how each operation has been implemented. Moreover we will propose two different crossover operations and at the end we will compare them in order to choose the best one. Since data have a specific constraint (sum of variables equals to 1), crossover we have implemented are not the usual ones. In order to respect the costraint they, in some way, include some mutation operations. In conclusion we will explain which of the two variants is the best.
\subsection{Selection}
During the selection operation we will choose 10 elements from the current popoulation that will be used to generate the new population. The two elements with higher value of fitness function will be brought directly to the mutation phase. Eight parents will be selected from the current population, using the roulette wheel method; then we will apply crossover on them.\\
\textbf{Roulette Wheel method} consists in evaluating the total fitness of the current population, assigning to each element of the population a probability that is proportional to the turbidity. In order to clarify this concept, we want to underline that elements with higher fitness value have higher probability to be selected.\\
Since, turbidity may have also negative value, it does not be considered as it is, instead we applied the Logistic function (L) to it:
\begin{equation}
L(Y) = \frac{1}{1 + e^{-2Y}}
\end{equation}
\begin{figure}[H]
\centering
\begin{tikzpicture}
\begin{axis}[xmin = -2,xmax = 2, ymin = -0.1, ymax = 1.1,width=10cm,height=5cm,axis x line=middle,axis y line=middle]
\addplot[samples=200,blue]{1/(1 + exp(-2*x))};
\addplot[dashed]{1};
\end{axis}
\end{tikzpicture}
\end{figure}
The probability will be distributed according to the following function:\\
\begin{equation}
p(X_i) = \frac{L(T(X_i))}{\sum_{i=1}^{10} L(T(X_i))}
\end{equation}
\subsection{Crossover}
Due to the problem constraints usual crossover methods cannot be directly applied; so we decided to perform two different crossover methods:
\begin{itemize}
	\item mean crossover;
	\item coin crossover.
\end{itemize}
\subsubsection{Mean Crossover}
Given two parents, we computed the mean on each corresponding unknown of them. Since an even number of unknown might have odd value, we adjust them by randomly adding a constant ($0.1$) to half of them and on the second half we subtract the same constant value. Viceversa for the second child.
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{images/Mean_Crossover.png}
	\caption{Example of mean crossover}
	\label{mean crossover}
\end{figure}
\subsubsection{Coin Crossover}
Given two parents, we randomly assign to each of them a certain number of coins. The total number of coins to be distributed among the two parents is 5. For each coin assigned to a parent, we randomly pick a constant value ($0.2$) from a certain unknown, which has to be present whithin the parent composition. We sum then the values picked to generate the first child. For the second child we swap the number of coins assigned to the parents and then repeat the same operation.
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{images/Coin_Crossover.png}
	\caption{Example of coin crossover}
	\label{coin crossover}
\end{figure}
\subsection{Mutation}
With respect to the mutation operation we distinguish between the two best solutions and the other children generated during the crossover operation.\\
\begin{itemize}
	\item In the first case we always apply mutation, even though we use different percentages on mean and coin crossover. In 40\% (50\% with coin crossover) of the times, we apply mutation only on the active components, thus, increasing and decreasing the their value by a constant ($0.2$). In the remaining 60\% (50\% with coin crossover) of the times, we apply mutation using the same constant value used in the previous case, choosing among all components, in such a way that the constraint still fulfilled.
	\item With respect to the eight children, we apply mutation on 20\% of the times considering all components, both active and inactive unknowns. 
Using these different kinds of mutation, we try to perform a good compromise between exploration an exploitation. The first one is given by the mutation in both active and inactive components, while the second is obtained by mutating only the active components.	
\end{itemize}
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{images/Mutation.png}
	\caption{}
	\label{mutation}
\end{figure}
\section{Results}
Now, we present the results obtained by the Genetic Algorithm developed. We discuss the two algorithm features (one for each crossover operation) and compare their result in order to show merits and defects of them. At the end we choose the best one and explain our considerations.
\subsection{Mean Crossover}
The histogram in Figure \ref{mean histogram} shows the turbidity of the elements of all the generations. Each generation is described by elements going from red to yellow. It highlights that the first random generation had some good values, that were immediately lost due to mutation. However the algorithm gradually increase the turbidity in all generations up to the 19\textsuperscript{th} generation where we reached the optimum $1.316926$, that was related to the composition made by $X7=0.6$ and $X14=0.4$, while all other components are inactive.
We can see that the mean value and the total sum increase, as we will see in detail below.
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm]{images/mean_1_generation_fitness.png}
	\caption{}
	\label{mean histogram}
\end{figure}
In the following figure we can find the boxblot of the turbidity in all the generations. It is a detail of the previous graphic, but we can see also the standard deviation.
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/mean_3_turbidity_distribution_at_each_generation.png}
	\caption{}
	\label{mean boxplot}
\end{figure}
We can deduce from the standard deviation that we perform a good exploration of the experimental space, which allows us to be independent from the initial generation.   
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/mean_4_turbidity_SD_at_each_generation.png}
	\caption{}
	\label{mean sd}
\end{figure}
As we can see from Figure \ref{mean average} the average fitness increases at each iteration, thus improving the whole generation and not just the best element. We do not report the graphic for the total sum on each iteration since the curve is exactly the same with a different scale. The maximum total sum we have obtained is around $7.0$.
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/mean_6_average_fitness_at_each_generation.png}
	\caption{}
	\label{mean average}
\end{figure}
As we already saw, we started from a good initial population, then we have got a decrease of the maximum fitness due to the mutation operation. Nevertheless after a couple of generations the maximum fitness starts to increase, reaching its optimum value. Anyway the fitness globally grows. 
\begin{figure}[H]	
\centering
	\includegraphics[width=14cm, height=7cm,]{images/mean_7_max_fitness_at_each_generation.png}
	\caption{}
	\label{mean max}
\end{figure}
The following figure shows us the generations' composition related to Turbidity. Each column represents a single generation, sectioned in coloured areas that represent the number of individuals in a specific range of turbidity values. In general the last generation have individuals that have fitness higher than $0.25$. 
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/mean_2_fitness_distribution.png}
	\caption{}
	\label{mean composition}
\end{figure}
Now we are going to analyze how the best individuals in each generation behave.
Figure \ref{mean variables boxplot} shows variables's distribution. Except for variables X7, X13 and X14, we notice that all other components did not give an important contribution. 
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/mean_8_variables_distribution.png}
	\caption{"\textcolor{red}{\#}" represent values of the optmimum}
	\label{mean variables boxplot}
\end{figure}
In the Figure \ref{mean variables} there are the number of times in which variable appeared within the best solution for each generation. Again only variables X7, X13 and X14 are often present.
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/mean_9_variables_frequency.png}
	\caption{}
	\label{mean variables}
\end{figure}
Figure \ref{mean variables 16} gives us details of each component for all the best individuals in all generations.
We can conclude that variables which gave an important contribute are X7, X14 and, with slightly less importance, X13. 
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/mean_10_variables.png}
	\caption{}
	\label{mean variables 16}
\end{figure}

\subsection{Coin Crossover}
Now lets consider results from the second algorithm which uses coin crossover, remembering that the initial population is the same of the previous method. The histogram in Figure \ref{coin histogram} shows the turbidity of the elements of all the generations. As in the previous case, the first random generation had some good values, that were immediately lost due to mutation. However the algorithm does not gradually increased the turbidity in all generations, even if at 18\textsuperscript{th} one we reached the optimum, that is the same already found in the previous algorithm.
We can see that the mean value and the total sum do not constantly increase, as we will see in detail below.
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm]{images/coin_1_generation_fitness.png}
	\caption{}
	\label{coin histogram}
\end{figure}
In the following figure we can find the boxblot of the turbidity in all the generations. It is a detail of the previous graphic, but we can see also the standard deviation. 
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/coin_3_turbidity_distribution_at_each_generation.png}
	\caption{}
	\label{coin boxplot}
\end{figure}
We can explain from the standard deviation that we perform a good and maybe to much exploration of the experimental space. Indeed, boxplots in the previous figure are very large and the standard devations reach higher value than the previuos result.
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/coin_4_turbidity_SD_at_each_generation.png}
	\caption{}
	\label{coin sd}
\end{figure}
As we can see from Figure \ref{coin average}, the average fitness does not increase at each iteration and it has large variability. We do not report the graphic for the total sum on each iteration since the curve is exactly the same with a different scale. The maximum total sum we have obtained is again around $7.0$.
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/coin_6_average_fitness_at_each_generation.png}
	\caption{}
	\label{coin average}
\end{figure}
As said before, we started from a good initial population, then we have got a decrease of the maximum fitness due to the mutation operation. Up to 12\textsuperscript{th} we do not observe substantial increasing of the function, then what looks like a local optimum is reached at 16\textsuperscript{th} generation followed by a negative peak. Then the function shows a sudden increase reaching the optimum. Nevertheless the increase is not constant and this is an index of a bad behaviour of the algorithm.
\begin{figure}[H]	
\centering
	\includegraphics[width=14cm, height=7cm,]{images/coin_7_max_fitness_at_each_generation.png}
	\caption{}
	\label{coin max}
\end{figure}
The following figure shows us the generations' composition related to Turbidy. As we have foreseen in the previous analysis, there is not a uniform increase of the total population turbidity even if the last generations reach the maximum.
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/coin_2_fitness_distribution.png}
	\caption{}
	\label{coin composition}
\end{figure}
Now we are going to analyze how the best individuals in each generation behave.
Figure \ref{coin variables boxplot} shows variables's distribution. In this case we notice that many variables have larger variability than in the first algorithm.
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/coin_8_variables_distribution.png}
	\caption{"\textcolor{red}{\#}" represent values of the optmimum}
	\label{coin variables boxplot}
\end{figure}
Below there are the number of times each variable appeared in the best solution for each generation. As we have seen in the previous figure a model does not clearly appear.
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/coin_9_variables_frequency.png}
	\caption{}
	\label{coin variables}
\end{figure}
Figure \ref{coin variables 16} gives the details of each component for all the best individuals in all generations. We cannot define which variables are the most influential.
\begin{figure}[H]
	\centering
	\includegraphics[width=14cm, height=7cm,]{images/coin_10_variables.png}
	\caption{}
	\label{coin variables 16}
\end{figure}

\section{Conclusions}
In this document we have shown how Genetic Algorithm can be used to search for the optmimal value of turbidity in an experimental space of compositions made by sixteen variables. Our Genetic Algorithm found the optmimum searching only in the $1.29\%$ of the possible combinations. The general structure of the algorithm we presented consists in the usual three operations of genetic algorithms which are selection, crossover and mutation. The first step, the selections, was developed in this way: the two best individuals of the population are taken as they are and the other eight with the roulette wheel method. Then on the eight previously selected parents we applied the crossover operation. Due to the particular constraint on the composition (sum equals to 1), we cannot apply the standard crossover methods, so we developed two new different techniques: the mean crossover and the coin crossover. We applied mutation operation on the two best solutions with probability $1$ and in the remaining cases with probability $0.2$. The two algorithms have been compared to find the best one. In conclusion we can say that the first method is obviously better than the second for some reasons, even if both reach the optimum value. The variability is smaller and it indicates that standard deviation in the second algorithm is too high. We can see this not only looking at the standard deviations values but also at variables distribution in the best individuals of each iteration. The populations' composition based on the turbidity value remarks this fact because also in last generations individuals with low turbidity value appear. Moreover, results of the previous method are in some way more constant because increasing rate of maximum, average and total fitness do not continuously change as in the second algorithm.\\
In both methods, even if more clearly in the first, we saw that variables X7 and X14 have a higher influence on the turbidity than the other ones. So we conclude that good solutions can be recognized using this sort of model.
\end{document}