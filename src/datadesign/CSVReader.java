package datadesign;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;
import java.util.ArrayList;


class CSVReader {
    
    public static final int NUM_ROWS = 15504;
    
    private final String csvFile;
    private final String cvsSplitBy;
    private final HashMap<String,Double> fitnessMap;
    private final String[] initialPopulation;
    private MersenneTwisterFast msf;
    
    private ArrayList<Integer> generatorRandom(int numElementsPopulation){
        int i;
        Integer actual;
        Random random = new Random(20);
        ArrayList<Integer> rnd_generated = new ArrayList<>();
        for(i = 0;i < numElementsPopulation; i++){
            do{
                actual = random.nextInt(NUM_ROWS) + 1;
            }while(rnd_generated.contains(actual));
            rnd_generated.add(actual);
        }
        return rnd_generated;
    }
    
    private ArrayList<Integer> generatorRandomMSF(int numElementsPopulation){
        int i;
        Integer actual;
        msf = new MersenneTwisterFast();
        ArrayList<Integer> rnd_generated = new ArrayList<>();
        for(i = 0;i < numElementsPopulation; i++){
            do{
                actual = msf.nextInt(NUM_ROWS) + 1;
            }while(rnd_generated.contains(actual));
            rnd_generated.add(actual);
        }
        return rnd_generated;
    }
    
    /**
     * @param path path of the csv file to load
     * @param splitChar character used in the csv to separate the column
     * @param numElementsPopulation number of columns that compose the string of a parent
     */
    public CSVReader(String path, String splitChar, int numElementsPopulation) {
        csvFile = path;
        cvsSplitBy = splitChar;
        fitnessMap = new HashMap<String, Double>();
        initialPopulation = new String[numElementsPopulation];
        BufferedReader br = null;
        String line;
        String inputMap = "";
        Double fitnessLine;
        int i;
        int actualLine = 1;
        int inserted = 0;
        ArrayList<Integer> rnd_generated = generatorRandom(numElementsPopulation);      

        try {

            br = new BufferedReader(new FileReader(csvFile));
            line = br.readLine();//read the first line that is the label of the columns
            while ((line = br.readLine()) != null) {

                String[] row = line.split(cvsSplitBy);
                
                //row.length-2 because I don't have to take into the string the value of the fitness function and in the last one element I don't want the ;
                for(i=1;i<row.length-2;i++){
                    inputMap += row[i] + ";";
                }
                inputMap += row[i];
                fitnessLine = Double.parseDouble(row[i+1].replace(",","."));
                
                fitnessMap.put(inputMap,fitnessLine);
                
                if(rnd_generated.contains(actualLine)){
                    initialPopulation[inserted] = inputMap;
                    inserted++;
                }
                
                inputMap = "";
                actualLine++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    
    public HashMap<String,Double> getMap(){
        return fitnessMap;
    }
    
    public String[] getInitialPopulation(){
        return initialPopulation;
    }

}
