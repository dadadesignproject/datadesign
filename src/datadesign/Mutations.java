package datadesign;

import java.util.HashMap;
import java.util.Random;
import java.util.regex.Matcher;

public class Mutations {
    private HashMap<String, Double> map;
    private MersenneTwisterFast msf;
    private int skipping_parents;
    double P_skipping_parents;
    double P_others;

    static int POP_SIZE = 10;
    static int ITE_NUM = 20;
    static int VAR_NUM = 16;

    public Mutations(HashMap<String, Double> map, int skipping_parents, double P_skipping_parents, double P_others) {
        this.map = map;
        this.skipping_parents = skipping_parents;
        this.P_skipping_parents = P_skipping_parents;
        this.P_others = P_others;
    }

    public String[] mutation(String[] actualPopulation, double p) {
        int i;
        int dimPopulation = actualPopulation.length;
        int dimParent;
        Double[] element;
        String[] resultPopulation = new String[dimPopulation];

        Random random = new Random();
        msf = new MersenneTwisterFast();
        double randomMutation;
        int randomPos1;
        int randomPos2;

        //mutation dei due parent migliori (fatta sempre)
        for(i = 0; i < 2; i++){
            element = DataDesign.stringToDouble(actualPopulation[i]);
            dimParent = element.length;

            //randomMutation = random.nextDouble() * 100.0;
            randomMutation = msf.nextDouble() * 100.0;
            if (randomMutation <= 30.0) {
                do{
                    //randomPos1 = random.nextInt(dimParent);
                    randomPos1 = msf.nextInt(dimParent);
                }while(element[randomPos1] == 0);

                do {
                    //randomPos2 = random.nextInt(dimParent);
                    randomPos2 = msf.nextInt(dimParent);
                } while (element[randomPos2]==0 || element[randomPos2] == 1 || randomPos1 == randomPos2);

                element[randomPos1] = Math.round((element[randomPos1] - 0.2)*10) / 10.0 ;
                element[randomPos2] = Math.round((element[randomPos2] + 0.2)*10) / 10.0 ;
            }
            else{
                do{
                    //randomPos1 = random.nextInt(dimParent);
                    randomPos1 = msf.nextInt(dimParent);
                }while(element[randomPos1] == 0);

                do {
                    //randomPos2 = random.nextInt(dimParent);
                    randomPos2 = msf.nextInt(dimParent);
                } while (element[randomPos2] == 1 || randomPos1 == randomPos2);

                element[randomPos1] = Math.round((element[randomPos1] - 0.2)*10) / 10.0 ;
                element[randomPos2] = Math.round((element[randomPos2] + 0.2)*10) / 10.0 ;
            }
            resultPopulation[i] = DataDesign.doubleToString(element);
        }

        for (i = 2; i < dimPopulation; i++) {
            element = DataDesign.stringToDouble(actualPopulation[i]);
            dimParent = element.length;

            //randomMutation = random.nextDouble() * 100.0;
            randomMutation = msf.nextDouble() * 100.0;
            if (randomMutation <= p) {/*con probabilità 30% faccio la mutation*/

                do{
                    //randomPos1 = random.nextInt(dimParent);
                    randomPos1 = msf.nextInt(dimParent);
                }while( element[randomPos1] == 0);

                do {
                    //randomPos2 = random.nextInt(dimParent);
                    randomPos2 = msf.nextInt(dimParent);
                } while (element[randomPos2] == 1 || randomPos1 == randomPos2);

                element[randomPos1] = Math.round((element[randomPos1] - 0.2)*10) / 10.0 ;
                element[randomPos2] = Math.round((element[randomPos2] + 0.2)*10) / 10.0 ;
            }
            resultPopulation[i] = DataDesign.doubleToString(element);

        }

        return resultPopulation;
    }

    // ADD BY YLE
    public String singleMutation(String element) {
        Double[] toMutate = DataDesign.stringToDouble(element);

        msf = new MersenneTwisterFast();

        double randomMutation = msf.nextDouble() * 100.0;
        if (randomMutation <= P_others) {
            int randomPos1;
            int randomPos2;
            do{
                randomPos1 = msf.nextInt(VAR_NUM);
            }while(toMutate[randomPos1] == 0);

            do {
                randomPos2 = msf.nextInt(VAR_NUM);
            } while ((toMutate[randomPos2] == 1 && toMutate[randomPos1] != 1) || randomPos1 == randomPos2);

            toMutate[randomPos1] = Math.round((toMutate[randomPos1] - 0.2)*10) / 10.0 ;
            toMutate[randomPos2] = Math.round((toMutate[randomPos2] + 0.2)*10) / 10.0 ;
        }
        return DataDesign.doubleToString(toMutate);
    }

    public String singleMutationSkippingParents(String element) {
        Double[] toMutate = DataDesign.stringToDouble(element);

        int randomPos1;
        int randomPos2;

        msf = new MersenneTwisterFast();

        double randomMutation = msf.nextDouble() * 100.0;
        if (randomMutation <= P_skipping_parents) {
            do{
                randomPos1 = msf.nextInt(VAR_NUM);
            }while(toMutate[randomPos1] == 0);

            do {
                randomPos2 = msf.nextInt(VAR_NUM);
            } while (toMutate[randomPos2]==0 || (toMutate[randomPos2] == 1 && toMutate[randomPos1] != 1) || (randomPos1 == randomPos2 && toMutate[randomPos1] != 1));

            toMutate[randomPos1] = Math.round((toMutate[randomPos1] - 0.2)*10) / 10.0 ;
            toMutate[randomPos2] = Math.round((toMutate[randomPos2] + 0.2)*10) / 10.0 ;
        }
        else{
            do{
                randomPos1 = msf.nextInt(VAR_NUM);
            }while(toMutate[randomPos1] == 0);

            do {
                //randomPos2 = random.nextInt(dimParent);
                randomPos2 = msf.nextInt(VAR_NUM);
            } while ((toMutate[randomPos2] == 1 && toMutate[randomPos1] != 1) || (randomPos1 == randomPos2 && toMutate[randomPos1] != 1));

            toMutate[randomPos1] = Math.round((toMutate[randomPos1] - 0.2)*10) / 10.0 ;
            toMutate[randomPos2] = Math.round((toMutate[randomPos2] + 0.2)*10) / 10.0 ;
        }

        return DataDesign.doubleToString(toMutate);
    }

    public String[] mutationBasic(String[] actualPopulation) {
        int i;
        Double[] element;
        String[] resultPopulation = new String[POP_SIZE];

        msf = new MersenneTwisterFast();

        for(i = 0; i < skipping_parents; i++){
            resultPopulation[i] = singleMutationSkippingParents(actualPopulation[i]);
        }

        for (i = skipping_parents; i < POP_SIZE; i++) {
            resultPopulation[i] = singleMutation(actualPopulation[i]);

        }

        return resultPopulation;
    }

    public double evaluateConstraint(Double[] individual) {
        double con = 0.0;

        for(int i = 0; i < VAR_NUM; ++i) {
            con = Math.round((con + individual[i]) * 10.0) / 10.0;
        }

        return con;
    }

    public String singleMutationToAdjust(String element) {
        Double[] toMutate = DataDesign.stringToDouble(element);
        double con = Math.round(evaluateConstraint(toMutate) * 10.0) / 10.0;
        int pos;

        msf = new MersenneTwisterFast();

        while(con > 1.0) {
            msf = new MersenneTwisterFast();
            do {
                pos = msf.nextInt(VAR_NUM);
            } while(toMutate[pos] == 0.0);

            toMutate[pos] = Math.round((toMutate[pos] - 0.2) * 10.0) / 10.0;
            con = Math.round((con - 0.2) * 10.0) / 10.0;
        }


        while(con < 1.0) {
            msf = new MersenneTwisterFast();
            do {
                pos = msf.nextInt(VAR_NUM);
            } while(toMutate[pos] == 1.0);

            toMutate[pos] = Math.round((toMutate[pos]+ 0.2) * 10.0) / 10.0;
            con = Math.round((con + 0.2) * 10.0) / 10.0;
        }


        return singleMutation(DataDesign.doubleToString(toMutate));
    }

    public String[] mutationToAdjustAll(String[] actualPopulation) {
        String[] resultPopulation = new String[POP_SIZE];

        for(int i = 0; i < skipping_parents; i++){
            resultPopulation[i] = singleMutationSkippingParents(actualPopulation[i]);
        }

        for(int i = skipping_parents; i < POP_SIZE; i++) {
            resultPopulation[i] = singleMutationToAdjust(actualPopulation[i]);
        }

        return resultPopulation;
    }
}
