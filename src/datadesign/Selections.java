package datadesign;

import java.util.*;

public class Selections {

    private HashMap<String, Double> map;
    private String[] parents;
    private int dimPopulation;
    private MersenneTwisterFast msf;

    private int skipping_parents;

    static int POP_SIZE = 10;

    public Selections(HashMap<String, Double> map, int skipping_parents) {
        this.map = map;
        this.skipping_parents = skipping_parents;
    }

    /*
     * Select two parents that has highest fitness
     */
    /*private void maximumFitness(String[] actualPopulation) {
        PriorityQueue<Integer> elements = new PriorityQueue<>(skipping_parents);
        //Double max_fitness = DataDesign.adjustFitness(map.get(actualPopulation[0]));
        Double max_fitness = map.get(actualPopulation[0]);
        Double second_max_fitness = -10.0;
        int pos_max = 0;
        int pos_second_max = -1;
        Double actual_fitness;
        for (int i = 1; i < dimPopulation; i++) {
            //actual_fitness = DataDesign.adjustFitness(map.get(actualPopulation[i]));
            actual_fitness = map.get(actualPopulation[i]);
            //System.out.println("Iterazione " + i + " max " + max_fitness + " 2max " + second_max_fitness + " ora " + actual_fitness);
            if (actual_fitness > max_fitness) {
                second_max_fitness = max_fitness;
                pos_second_max = pos_max;
                max_fitness = actual_fitness;
                pos_max = i;
            } else if (actual_fitness > second_max_fitness) {
                second_max_fitness = actual_fitness;
                pos_second_max = i;
            }
        }
        parents[0] = actualPopulation[pos_max];
        parents[1] = actualPopulation[pos_second_max];
    }*/

    private void maximumFitness(String[] actualPopulation) {
        Comparator<String> c = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if(map.get(o1)>map.get(o2))
                    return -1;
                else if(map.get(o1)<map.get(o2))
                    return 1;
                return 0;
            }
        };
        PriorityQueue<String> elements = new PriorityQueue<>(c);

        for(int i = 0; i < POP_SIZE; i++){
            elements.add(actualPopulation[i]);
        }

        for(int i = 0; i < skipping_parents; i++){
            parents[i] = elements.poll();
        }
    }

    /*
    public String[] roulette(String[] actualPopulation) {
        dimPopulation = actualPopulation.length;
        parents = new String[dimPopulation];
        Double totalFitness = 0.0;
        Double currentFitness;
        Double[] rouletteSelection = new Double[dimPopulation];
        int i, j;
        msf = new MersenneTwisterFast();
        Random random = new Random();
        Double doubleRandom;

        //Creo la roulette
        for (i = 0; i < dimPopulation; i++) {
            currentFitness = DataDesign.adjustFitness(map.get(actualPopulation[i]));
            totalFitness += currentFitness;
            rouletteSelection[i] = currentFitness;
        }

        /*System.out.println("");
         System.out.println("probabilità");
         for(i = 0; i < dimPopulation; i++){
         System.out.print(rouletteSelection[i]);
         }
         System.out.println("");
         COMMENTA
        rouletteSelection[0] = rouletteSelection[0] / totalFitness;
        for (i = 1; i < dimPopulation; i++) {
            rouletteSelection[i] = rouletteSelection[i - 1] + rouletteSelection[i] / totalFitness;
        }

        maximumFitness(actualPopulation);//seleziono 2 parent con la fitness più alta
        for (i = 2; i < dimPopulation; i++) {//i primi due sono scelti come i parents con fitness più alta
            //doubleRandom = random.nextDouble();
            doubleRandom = msf.nextDouble();
            j = 0;
            while (j < dimPopulation - 1 && rouletteSelection[j] < doubleRandom) {
                j++;
            }
            parents[i] = actualPopulation[j];
            /*if ((i + 1) % 2 == 1) {
                int k = 100;
                while (k>0 && parents[i - 1].equals(parents[i])) {
                    doubleRandom = random.nextDouble();
                    j = 0;
                    while (j < dimPopulation - 1 && rouletteSelection[j] < doubleRandom) {
                        j++;
                    }
                    parents[i] = actualPopulation[j];
                    k--;
                }
            }
            COMMENTA

        }

        /*System.out.println("");
         System.out.println("corrente");
         System.out.println(actualPopulation[0] + "    " + map.get(actualPopulation[0]) + "->" + rouletteSelection[0]);
         for(i = 1; i < dimPopulation; i++){
         System.out.println(actualPopulation[i] + "    " + map.get(actualPopulation[i]) + "->" + (rouletteSelection[i] - rouletteSelection[i-1]) + "  ");
         }
        
         System.out.println("");
         System.out.println("Parents della selection");
         for(i = 0; i < dimPopulation; i++){
         System.out.println(parents[i] + "    " + map.get(parents[i]));
         }
         COMMENTA
        return parents;
    }*/

    public String[] roulette(String[] actualPopulation) {
        parents = new String[POP_SIZE];

        Double totalFitness = 0.0;
        Double currentFitness;
        Double[] rouletteSelection = new Double[POP_SIZE];

        int i, j;
        msf = new MersenneTwisterFast();

        Double doubleRandom;

        /*Creo la roulette*/
        for (i = 0; i < POP_SIZE; i++) {
            currentFitness = DataDesign.adjustFitness(map.get(actualPopulation[i]));
            totalFitness += currentFitness;
            rouletteSelection[i] = currentFitness;
        }

        rouletteSelection[0] = rouletteSelection[0] / totalFitness;
        for (i = 1; i < POP_SIZE; i++) {
            rouletteSelection[i] = rouletteSelection[i - 1] + rouletteSelection[i] / totalFitness;
        }

        maximumFitness(actualPopulation);

        for (i = skipping_parents; i < POP_SIZE; i++) {//i primi due sono scelti come i parents con fitness più alta
            doubleRandom = msf.nextDouble();
            j = 0;
            while (j < POP_SIZE - 1 && rouletteSelection[j] < doubleRandom) {
                j++;
            }
            parents[i] = actualPopulation[j];

        }
        return parents;
    }

    /*public String[] rank(String[] actualPopulation) {
        dimPopulation = actualPopulation.length;
        parents = new String[dimPopulation];
        int i, j;
        Double[] rankSelection = new Double[dimPopulation];
        Random random = new Random();
        msf = new MersenneTwisterFast();
        Double doubleRandom;

        //Creo il vettore con i rank
        for (i = 0; i < dimPopulation; i++) {
            rankSelection[i] = DataDesign.adjustFitness(map.get(actualPopulation[i]));
        }

        //sortiamo l'array
        for (i = 0; i < rankSelection.length - 1; i++) {
            int minimo = i; //Partiamo dall' i-esimo elemento
            for (j = i + 1; j < rankSelection.length; j++) {
                if (rankSelection[minimo] > rankSelection[j]) {
                    minimo = j;
                }
            }

            if (minimo != i) {
                Double k = rankSelection[minimo];
                String s = actualPopulation[minimo];
                rankSelection[minimo] = rankSelection[i];
                actualPopulation[minimo] = actualPopulation[i];
                rankSelection[i] = k;
                actualPopulation[i] = s;
            }
        }

        rankSelection[0] = (1.0) * (1.0 / 55.0);
        for (i = 1; i < dimPopulation; i++) {
            rankSelection[i] = rankSelection[i - 1] + (i + 1.0) * (1.0 / 55.0);
        }

        for (i = 0; i < dimPopulation; i++) {
            System.out.println(actualPopulation[i] + "->" + rankSelection[i] + "->" + map.get(actualPopulation[i]));
        }
        System.out.println("");

        for (i = 0; i < dimPopulation; i++) {//i primi due sono scelti come i parents con fitness più alta
            //doubleRandom = random.nextDouble();
            doubleRandom = msf.nextDouble();
            j = 0;
            while (j < dimPopulation - 1 && rankSelection[j] < doubleRandom) {
                j++;
            }
            parents[i] = actualPopulation[j];
        }

        System.out.println("");
        System.out.println("Parents della selection");
        for (i = 0; i < dimPopulation; i++) {
            System.out.println(parents[i] + "    " + map.get(parents[i]));
        }
        return parents;
    }*/

    public String[] rank(String[] actualPopulation) {
        parents = new String[POP_SIZE];
        int i, j;
        Double[] rankSelection = new Double[POP_SIZE];

        msf = new MersenneTwisterFast();

        Double doubleRandom;

        /*Creo il vettore con i rank*/
        for (i = 0; i < POP_SIZE; i++) {
            rankSelection[i] = DataDesign.adjustFitness(map.get(actualPopulation[i]));
        }

        //sortiamo l'array
        for (i = 0; i < POP_SIZE - 1; i++) {
            int minimo = i; //Partiamo dall' i-esimo elemento
            for (j = i + 1; j < POP_SIZE; j++) {
                if (rankSelection[minimo] > rankSelection[j]) {
                    minimo = j;
                }
            }

            if (minimo != i) {
                Double k = rankSelection[minimo];
                String s = actualPopulation[minimo];
                rankSelection[minimo] = rankSelection[i];
                actualPopulation[minimo] = actualPopulation[i];
                rankSelection[i] = k;
                actualPopulation[i] = s;
            }
        }

        rankSelection[0] = (1.0) * (1.0 / 55.0);
        for (i = 1; i < POP_SIZE; i++) {
            rankSelection[i] = rankSelection[i - 1] + (i + 1.0) * (1.0 / 55.0);
        }

        for (i = 0; i < POP_SIZE; i++) {
            System.out.println(actualPopulation[i] + "->" + rankSelection[i] + "->" + map.get(actualPopulation[i]));
        }
        System.out.println("");

        maximumFitness(actualPopulation);

        for (i = skipping_parents; i < POP_SIZE; i++) {//i primi due sono scelti come i parents con fitness più alta
            doubleRandom = msf.nextDouble();
            j = 0;
            while (j < POP_SIZE - 1 && rankSelection[j] < doubleRandom) {
                j++;
            }
            parents[i] = actualPopulation[j];
        }

        System.out.println("");
        System.out.println("Parents della selection");
        for (i = 0; i < POP_SIZE; i++) {
            System.out.println(parents[i] + "    " + map.get(parents[i]));
        }
        return parents;
    }
}
