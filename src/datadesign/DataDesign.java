package datadesign;

import java.util.HashMap;

public class DataDesign {
    static HashMap<String,Double> MAP;
    static CSVReader CSV;
    static String[] INITIAL;
    static int POP_SIZE = 10;
    static int ITE_NUM = 20;
    static int VAR_NUM = 16;

    public static Double adjustFitness(Double fit){
        return 1/(1+Math.pow(Math.E,-2*fit));
        //return Math.pow(fit + 3.0,50);
    }
    
    /*
    Prende una stringa rappresentante un genitore e la converte in array di Double
    */
    public static Double[] stringToDouble(String s) {
        String[] support = s.split(";");
        Double[] result = new Double[support.length];
        for (int i = 0; i < support.length; i++) {
            result[i] = Double.parseDouble(support[i].replace(",", "."));
        }
        return result;
    }
    
    /*
    Prende un array di Double e lo converte in stringa in cui ogni elemento dell'array è separato da un ;
     */
    public static String doubleToString(Double[] d) {
        String result = "";
        int i;
        for (i = 0; i < d.length; i++) {
            if (d[i] == 0.0) {
                result += "0;";
            } else if (d[i] == 1.0) {
                result += "1;";
            } else {
                result += d[i].toString().replace(".", ",") + ";";
            }
        }
        return result.substring(0, result.length() - 1);
    }
    
    private static void printPopulation(String[] v, HashMap<String,Double> map){
        for(int i=0;i<v.length;i++){
            //System.out.println(v[i] + "     " + adjustFitness(map.get(v[i])));
            System.out.println(v[i] + "         "+ map.get(v[i]));
        }
    }

    public static void methodMeanCrossOverWithMutation(int skipping_parents, double P_skipping_parents, double P_others, String fileName){
        Selections s = new Selections(MAP, skipping_parents);
        Crossovers c = new Crossovers(MAP, skipping_parents);
        Mutations m = new Mutations(MAP, skipping_parents, P_skipping_parents, P_others);
        String[] actualPopulation;

        CSVWriter.openWriter(fileName);
        CSVWriter.writeHeader();
        CSVWriter.writeOnCsvFile(INITIAL, MAP,-1);

        actualPopulation = INITIAL;

        for(int i = 0; i < ITE_NUM; i++){
            actualPopulation = s.roulette(actualPopulation);
            actualPopulation = c.meanCrossover(actualPopulation);
            actualPopulation = m.mutationBasic(actualPopulation);
            CSVWriter.writeOnCsvFile(actualPopulation, MAP,i);
        }

        CSVWriter.closeWriter();
    }

    public static void methodCoinCrossOverWithMutation(int skipping_parents, double P_skipping_parents, double P_others, String fileName){
        Selections s = new Selections(MAP, skipping_parents);
        Crossovers c = new Crossovers(MAP, skipping_parents);
        Mutations m = new Mutations(MAP, skipping_parents, P_skipping_parents, P_others);
        String[] actualPopulation;

        CSVWriter.openWriter(fileName);
        CSVWriter.writeHeader();
        CSVWriter.writeOnCsvFile(INITIAL, MAP,-1);

        actualPopulation = INITIAL;

        for(int i = 0; i < ITE_NUM; i++){
            actualPopulation = s.roulette(actualPopulation);
            actualPopulation = c.coinCrossover(actualPopulation);
            actualPopulation = m.mutationBasic(actualPopulation);
            CSVWriter.writeOnCsvFile(actualPopulation, MAP,i);
        }

        CSVWriter.closeWriter();
    }

    /**
     *
     * @param N
     * @param skipping_parents
     * @param P_skipping_parents
     * @param P_others
     * @param fileName
     */
    public static void methodCutCrossOverWithMutation(int N, int skipping_parents, double P_skipping_parents, double P_others, String fileName){
        Selections s = new Selections(MAP, skipping_parents);
        Crossovers c = new Crossovers(MAP, skipping_parents);
        Mutations m = new Mutations(MAP, skipping_parents, P_skipping_parents, P_others);
        String[] actualPopulation;

        CSVWriter.openWriter(fileName);
        CSVWriter.writeHeader();
        CSVWriter.writeOnCsvFile(INITIAL, MAP,-1);

        actualPopulation = INITIAL;

        for(int i = 0; i < ITE_NUM; i++){
            actualPopulation = s.roulette(actualPopulation);
            actualPopulation = c.cutCrossover(actualPopulation, N);
            actualPopulation = m.mutationToAdjustAll(actualPopulation);
            CSVWriter.writeOnCsvFile(actualPopulation, MAP,i);
        }

        CSVWriter.closeWriter();
    }

    public static void initialize(){
        CSV = new CSVReader("space_response.csv",";",10);
        MAP = CSV.getMap();
        INITIAL = CSV.getInitialPopulation();
    }

    /**
     * @param args the command line arguments
     *
     */
    public static void main(String[] args){
        initialize();

        for(int i = 0; i < 9; i++){
            methodMeanCrossOverWithMutation(2, 40.0, 20.0, "output_" + (i + 1) + ".csv");
        }

        for(int i = 0; i < 9; i++){
            methodCoinCrossOverWithMutation(2, 50.0, 20.0, "output_" + (i + 1 + 10) + ".csv");
        }

        for(int i = 0; i < 9; i++){
            methodCutCrossOverWithMutation(4, 2, 40.0, 5.0, "output_" + (i + 1 + 20) + ".csv");
        }
    }

    /*public static void main(String[] args) {
        CSVReader csv = new CSVReader("space_response.csv",";",10);
        HashMap<String,Double> map= csv.getMap();
        String[] initial = csv.getInitialPopulation();
        Selections s = new Selections(map);
        Crossovers c = new Crossovers(map);
        Mutations m = new Mutations(map);
        String[] actualPopulation;//Variabile su cui scrivere la popolazione attuale per non sovrascrivere quella iniziale
        
        //printPopulation(initial,map);

        CSVWriter.openWriter("output_1.csv");
        CSVWriter.writeHeader();
        CSVWriter.writeOnCsvFile(initial, map,-1);
        //20 iterazioni con roulette - mean crossover e mutation
        actualPopulation = initial;
        for(int i = 0; i < 20; i++){
            actualPopulation = s.roulette(actualPopulation);
            actualPopulation = c.meanCrossover(actualPopulation);
            //printPopulation(actualPopulation,map);
            actualPopulation = m.mutation(actualPopulation, 20.0);
            //printPopulation(actualPopulation,map);
            CSVWriter.writeOnCsvFile(actualPopulation, map,i);
        }

        System.out.println("Popolazione finale roulette - mean crossover e mutation");
        printPopulation(actualPopulation,map);
        CSVWriter.closeWriter();
        
        CSVWriter.openWriter("output_2.csv");
        CSVWriter.writeHeader();
        CSVWriter.writeOnCsvFile(initial, map,-1);
        //20 iterazioni con roulette - mean crossover
        actualPopulation = initial;
        
        for(int i = 0; i < 20; i++){
            actualPopulation = s.roulette(actualPopulation);
            actualPopulation = c.meanCrossover(actualPopulation);
            //printPopulation(actualPopulation,map);
            CSVWriter.writeOnCsvFile(actualPopulation, map,i);      
        }
        System.out.println("");
        System.out.println("Popolazione finale roulette - mean crossover");
        printPopulation(actualPopulation,map);
        CSVWriter.closeWriter();
        
        CSVWriter.openWriter("output_3.csv");
        CSVWriter.writeHeader();
        CSVWriter.writeOnCsvFile(initial, map,-1);
        //20 iterazioni con roulette - coin crossover e mutation
        actualPopulation = initial;
        
        for(int i = 0; i < 20; i++){
            actualPopulation = s.roulette(actualPopulation);
            actualPopulation = c.coinCrossover(actualPopulation);
            //printPopulation(actualPopulation,map);
            actualPopulation = m.mutation(actualPopulation, 20.0);
            //printPopulation(actualPopulation,map);
            CSVWriter.writeOnCsvFile(actualPopulation, map,i);      
        }
        System.out.println("");
        System.out.println("Popolazione finale roulette - coin crossover e mutation");
        printPopulation(actualPopulation,map);
        CSVWriter.closeWriter();
        
        CSVWriter.openWriter("output_4.csv");
        CSVWriter.writeHeader();
        CSVWriter.writeOnCsvFile(initial, map,-1);
        //20 iterazioni con roulette - coin crossover
        actualPopulation = initial;
        
        for(int i = 0; i < 20; i++){
            actualPopulation = s.roulette(actualPopulation);
            actualPopulation = c.coinCrossover(actualPopulation);
            //printPopulation(actualPopulation,map);
            CSVWriter.writeOnCsvFile(actualPopulation, map,i);      
        }
        System.out.println("");
        System.out.println("Popolazione finale roulette - coin crossover");
        printPopulation(actualPopulation,map);
        CSVWriter.closeWriter();
        
        //actualPopulation = s.rank(initial);
        
    }*/

}
