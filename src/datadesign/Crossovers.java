package datadesign;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.ArrayList;

class Crossovers {

    private HashMap<String, Double> map;
    private MersenneTwisterFast msf;
    private int skipping_parents;

    static int POP_SIZE = 10;
    static int VAR_NUM = 16;

    public Crossovers(HashMap<String, Double> map, int skipping_parents) {
        this.map = map;
        this.skipping_parents = skipping_parents;
    }

    //TODO cambiare il modo in cui incrementiamo decrementiamo il child se dispari
    /*public String[] meanCrossover(String[] actualPopulation) {
        int i, j, pos;
        int dimPopulation = actualPopulation.length;
        int dimParent = DataDesign.stringToDouble(actualPopulation[0]).length;
        String[] resultPopulation = new String[dimPopulation];
        Double[] parent1;
        Double[] parent2;
        Double[] child1 = new Double[dimParent];
        Double[] child2 = new Double[dimParent];

        double constant = 0.1;
        ArrayList<Integer> childOdd = new ArrayList<Integer>();
        int numberChildOdd;

        Random random = new Random();
        msf = new MersenneTwisterFast();

        //seleziono 2 parent con la fitness più alta
        resultPopulation[0] = actualPopulation[0];
        resultPopulation[1] = actualPopulation[1];

        for (i = 2; i < dimPopulation; i = i + 2) {
            parent1 = DataDesign.stringToDouble(actualPopulation[i]);
            parent2 = DataDesign.stringToDouble(actualPopulation[i + 1]);

            //A questo punto devo fare la somma di ogni elemento dei due parent e farne la media (divido per 2)
            for (j = 0; j < dimParent; j++) {
                double d = (parent1[j] + parent2[j]) / 2;
                child1[j] = Math.round(d * 10.0) / 10.0;
                child2[j] = child1[j];
                if ((child1[j] * 10.0) % 2.0 != 0.0) {//Se il child risulta essere un numero dispari lo devo portare ad essere pari
                    //System.out.println(i + " Pos " + j + " è dispri " + child1[j]);
                    childOdd.add(j);
                }
            }

            numberChildOdd = childOdd.size();
            //System.out.println("numberChildOdd = " + numberChildOdd);
            for (j = 0; j < numberChildOdd / 2; j++) {
                //pos = childOdd.remove(random.nextInt(childOdd.size()));
                pos = childOdd.remove(msf.nextInt(childOdd.size()));
                //System.out.println("Position to increment " + pos);
                child1[pos] = Math.round((child1[pos] + constant) * 10.0) / 10.0;
                child2[pos] = Math.round((child2[pos] - constant) * 10.0) / 10.0;
            }
            while (childOdd.size() != 0) {
                pos = childOdd.remove(0);
                //System.out.println("Position to decrement " + pos);
                child2[pos] = Math.round((child2[pos] + constant) * 10.0) / 10.0;
                child1[pos] = Math.round((child1[pos] - constant) * 10.0) / 10.0;
            }
            //System.out.println("");

            //Converto in stringa i due figli appena creati e li salvo in result population
            resultPopulation[i] = DataDesign.doubleToString(child1);
            resultPopulation[i + 1] = DataDesign.doubleToString(child2);
        }
        return resultPopulation;
    }*/

    public String[] meanCrossover(String[] actualPopulation) {
        int i, j, pos;
        String[] resultPopulation = new String[POP_SIZE];
        Double[] parent1;
        Double[] parent2;
        Double[] child1 = new Double[VAR_NUM];
        Double[] child2 = new Double[VAR_NUM];

        double constant = 0.1;
        ArrayList<Integer> childOdd = new ArrayList<Integer>();
        int numberChildOdd;

        Random random = new Random();
        msf = new MersenneTwisterFast();

        //seleziono 2 parent con la fitness più alta
        for(i = 0; i < skipping_parents; i++){
            resultPopulation[i] = actualPopulation[i];
        }

        for (i = skipping_parents; i < POP_SIZE; i = i + 2) {
            parent1 = DataDesign.stringToDouble(actualPopulation[i]);
            parent2 = DataDesign.stringToDouble(actualPopulation[i + 1]);

            /*A questo punto devo fare la somma di ogni elemento dei due parent e farne la media (divido per 2)*/
            for (j = 0; j < VAR_NUM; j++) {
                double d = (parent1[j] + parent2[j]) / 2;
                child1[j] = Math.round(d * 10.0) / 10.0;
                child2[j] = child1[j];
                if ((child1[j] * 10.0) % 2.0 != 0.0) {/*Se il child risulta essere un numero dispari lo devo portare ad essere pari*/
                    //System.out.println(i + " Pos " + j + " è dispri " + child1[j]);
                    childOdd.add(j);
                }
            }

            numberChildOdd = childOdd.size();
            //System.out.println("numberChildOdd = " + numberChildOdd);

            for (j = 0; j < numberChildOdd / 2; j++) {
                //pos = childOdd.remove(random.nextInt(childOdd.size()));
                pos = childOdd.remove(msf.nextInt(childOdd.size()));
                //System.out.println("Position to increment " + pos);
                child1[pos] = Math.round((child1[pos] + constant) * 10.0) / 10.0;
                child2[pos] = Math.round((child2[pos] - constant) * 10.0) / 10.0;
            }

            while (childOdd.size() != 0) {
                pos = childOdd.remove(0);
                //System.out.println("Position to decrement " + pos);
                child2[pos] = Math.round((child2[pos] + constant) * 10.0) / 10.0;
                child1[pos] = Math.round((child1[pos] - constant) * 10.0) / 10.0;
            }
            //System.out.println("");

                /*Converto in stringa i due figli appena creati e li salvo in result population*/
            resultPopulation[i] = DataDesign.doubleToString(child1);
            resultPopulation[i + 1] = DataDesign.doubleToString(child2);
        }

        return resultPopulation;
    }

    /*public String[] coinCrossover(String[] actualPopulation) {
        int i, j;
        int dimPopulation = actualPopulation.length;
        int dimParent = DataDesign.stringToDouble(actualPopulation[0]).length;
        String[] resultPopulation = new String[dimPopulation];
        Double[] parent1;
        Double[] parent2;
        Double[] child1 = new Double[dimParent];
        Double[] child2 = new Double[dimParent];
        ArrayList<Integer> positiveParent1Pos = new ArrayList<>();
        ArrayList<Integer> positiveParent2Pos = new ArrayList<>();

        Random random = new Random();
        msf = new MersenneTwisterFast();
        int randomCoin;
        int randomPos;

        //seleziono 2 parent con la fitness più alta (la selection me li restituisce in posizione 0 e 1)
        resultPopulation[0] = actualPopulation[0];
        resultPopulation[1] = actualPopulation[1];

        for (i = 2; i < dimPopulation; i = i + 2) {
            parent1 = DataDesign.stringToDouble(actualPopulation[i]);
            parent2 = DataDesign.stringToDouble(actualPopulation[i + 1]);

            for (int k = 0; k < dimParent; k++) {
                child1[k] = 0.0;
                child2[k] = 0.0;
            }

            //randomCoin = random.nextInt(6);
            randomCoin = msf.nextInt(6);
            int coinParent1 = randomCoin;
            int coinParent2 = 5 - randomCoin;

            //Construction of positive parents posotion lists
            for (int k = 0; k < parent1.length; k++) {
                if (parent1[k] > 0) {
                    positiveParent1Pos.add(k);
                }
            }

            for (int k = 0; k < parent2.length; k++) {
                if (parent2[k] > 0) {
                    positiveParent2Pos.add(k);
                }
            }
            
            while (coinParent1 > 0) {
                //randomPos = random.nextInt(dimParent);
                //randomPos = msf.nextInt(dimParent);
                randomPos = positiveParent1Pos.get( msf.nextInt(positiveParent1Pos.size()));
                /*
                if (parent1[randomPos] > 0) {
                    parent1[randomPos] = parent1[randomPos] - 0.2;
                    child1[randomPos] = Math.round((child1[randomPos] + 0.2) * 10) / 10.0;
                    coinParent1--;
                }
                COMMENTA
                parent1[randomPos] = parent1[randomPos] - 0.2;
                child1[randomPos] = Math.round((child1[randomPos] + 0.2) * 10) / 10.0;
                coinParent1--;
            }

            while (coinParent2 > 0) {
                //randomPos = random.nextInt(dimParent);
                randomPos = positiveParent2Pos.get(msf.nextInt(positiveParent2Pos.size()));
                parent2[randomPos] = parent2[randomPos] - 0.2;
                child1[randomPos] = Math.round((child1[randomPos] + 0.2) * 10) / 10.0;
                coinParent2--;
            }
            

            coinParent1 = 5 - randomCoin;
            coinParent2 = randomCoin;

            while (coinParent1 > 0) {
                //randomPos = random.nextInt(dimParent);
                /*
                randomPos = msf.nextInt(dimParent);
                if (parent1[randomPos] > 0) {
                    parent1[randomPos] = parent1[randomPos] - 0.2;
                    child2[randomPos] = Math.round((child2[randomPos] + 0.2) * 10) / 10.0;
                    coinParent1--;
                }
                COMMENTA
                randomPos = positiveParent1Pos.get(msf.nextInt(positiveParent1Pos.size()));
                parent1[randomPos] = parent1[randomPos] - 0.2;
                child2[randomPos] = Math.round((child2[randomPos] + 0.2) * 10) / 10.0;
                coinParent1--;
            }

            while (coinParent2 > 0) {
                //randomPos = random.nextInt(dimParent);
                /*
                randomPos = msf.nextInt(dimParent);
                if (parent2[randomPos] > 0) {
                    parent2[randomPos] = parent2[randomPos] - 0.2;
                    child2[randomPos] = Math.round((child2[randomPos] + 0.2) * 10) / 10.0;
                    coinParent2--;
                }
                COMMENTA
                randomPos = positiveParent2Pos.get(msf.nextInt(positiveParent2Pos.size()));
                parent2[randomPos] = parent2[randomPos] - 0.2;
                child2[randomPos] = Math.round((child2[randomPos] + 0.2) * 10) / 10.0;
                coinParent2--;
            }

            resultPopulation[i] = DataDesign.doubleToString(child1);
            resultPopulation[i + 1] = DataDesign.doubleToString(child2);
            positiveParent1Pos.clear();
            positiveParent2Pos.clear();
        }
        return resultPopulation;
    }*/

    public String[] coinCrossover(String[] actualPopulation) {
        int i;
        String[] resultPopulation = new String[POP_SIZE];
        Double[] parent1;
        Double[] parent2;
        Double[] child1 = new Double[VAR_NUM];
        Double[] child2 = new Double[VAR_NUM];
        ArrayList<Integer> positiveParent1Pos = new ArrayList<>();
        ArrayList<Integer> positiveParent2Pos = new ArrayList<>();

        msf = new MersenneTwisterFast();

        int randomCoin;
        int randomPos;

        //seleziono 2 parent con la fitness più alta (la selection me li restituisce in posizione 0 e 1)
        for(i = 0; i < skipping_parents; i++){
            resultPopulation[i] = actualPopulation[i];
        }

        for (i = skipping_parents; i < POP_SIZE; i = i + 2) {
            parent1 = DataDesign.stringToDouble(actualPopulation[i]);
            parent2 = DataDesign.stringToDouble(actualPopulation[i + 1]);

            for (int k = 0; k < VAR_NUM; k++) {
                child1[k] = 0.0;
                child2[k] = 0.0;
            }

            //randomCoin = random.nextInt(6);
            randomCoin = msf.nextInt(6);
            int coinParent1 = randomCoin;
            int coinParent2 = 5 - randomCoin;

            //Construction of positive parents posotion lists
            for (int k = 0; k < VAR_NUM; k++) {
                if (parent1[k] > 0) {
                    positiveParent1Pos.add(k);
                }
            }

            for (int k = 0; k < VAR_NUM; k++) {
                if (parent2[k] > 0) {
                    positiveParent2Pos.add(k);
                }
            }

            while (coinParent1 > 0) {
                randomPos = positiveParent1Pos.get( msf.nextInt(positiveParent1Pos.size()));
                parent1[randomPos] = parent1[randomPos] - 0.2;
                child1[randomPos] = Math.round((child1[randomPos] + 0.2) * 10) / 10.0;
                coinParent1--;
            }

            while (coinParent2 > 0) {
                randomPos = positiveParent2Pos.get(msf.nextInt(positiveParent2Pos.size()));
                parent2[randomPos] = parent2[randomPos] - 0.2;
                child1[randomPos] = Math.round((child1[randomPos] + 0.2) * 10) / 10.0;
                coinParent2--;
            }


            coinParent1 = 5 - randomCoin;
            coinParent2 = randomCoin;

            while (coinParent1 > 0) {
                randomPos = positiveParent1Pos.get(msf.nextInt(positiveParent1Pos.size()));
                parent1[randomPos] = parent1[randomPos] - 0.2;
                child2[randomPos] = Math.round((child2[randomPos] + 0.2) * 10) / 10.0;
                coinParent1--;
            }

            while (coinParent2 > 0) {
                randomPos = positiveParent2Pos.get(msf.nextInt(positiveParent2Pos.size()));
                parent2[randomPos] = parent2[randomPos] - 0.2;
                child2[randomPos] = Math.round((child2[randomPos] + 0.2) * 10) / 10.0;
                coinParent2--;
            }

            resultPopulation[i] = DataDesign.doubleToString(child1);
            resultPopulation[i + 1] = DataDesign.doubleToString(child2);

            positiveParent1Pos.clear();
            positiveParent2Pos.clear();
        }
        return resultPopulation;
    }

    public String[] cutCrossover(String[] actualPopulation, int N) {

        String[] resultPopulation = new String[POP_SIZE];

        Double[] child1 = new Double[VAR_NUM];
        Double[] child2 = new Double[VAR_NUM];

        Integer[] randomPos = new Integer[N + 1];

        msf = new MersenneTwisterFast();

        int k, t;

        for(int i = 0; i < skipping_parents; i++){
            resultPopulation[i] = actualPopulation[i];
        }

        for(int i = skipping_parents; i < POP_SIZE; i += 2) {

            Double[] parent1 = DataDesign.stringToDouble(actualPopulation[i]);
            Double[] parent2 = DataDesign.stringToDouble(actualPopulation[i + 1]);

            for(k = 0; k < VAR_NUM; ++k) {
                child1[k] = 0.0;
                child2[k] = 0.0;

            }

            k = 0;

            while(k < N) {
                boolean presente;
                do {
                    presente = false;
                    randomPos[k] = msf.nextInt(VAR_NUM - 1) + 1;
                    t = 0;
                    while (!presente && t < k) {
                        if (randomPos[t] == randomPos[k]) {
                            presente = true;

                        }
                        t++;
                    }
                } while (presente);
                k++;
            }

            randomPos[N] = VAR_NUM;

            Arrays.sort(randomPos);

            int toWrite = 0;

            for(k = 0; k < N + 1; ++k) {

                while(toWrite < randomPos[k]) {

                    if(k % 2 == 0) {
                        child1[toWrite] = parent1[toWrite];
                        child2[toWrite] = parent2[toWrite];
                    } else {
                        child1[toWrite] = parent2[toWrite];
                        child2[toWrite] = parent1[toWrite];
                    }
                    toWrite++;
                }
            }

            resultPopulation[i] = DataDesign.doubleToString(child1);
            resultPopulation[i + 1] = DataDesign.doubleToString(child2);
        }

        return resultPopulation;
    }

}
