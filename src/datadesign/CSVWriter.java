package datadesign;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class CSVWriter {

    private static BufferedWriter bw;
    private static String FILENAME;
    private static File F;
    private static final String HEADER = "iteration;X1;X2;X3;X4;X5;X6;X7;X8;X9;X10;X11;X12;X13;X14;X15;X16;Y";

    /**
     * @param name name of the resulting file
     */
    public static void openWriter(String name) {
        try {
            FILENAME = name;
            F = new File(FILENAME);
            bw = new BufferedWriter(new FileWriter(F));
        } catch (IOException ex) {
        }
    }

    public static void closeWriter() {
        try {
            bw.close();
        } catch (IOException e) {

        }
    }

    public static void writeHeader() {
        try {
            bw.write(HEADER);
            bw.newLine();
        } catch (IOException ex) {
        }
    }

    /**
     * Write into file the actual population
     * @param population actual population to print into file
     * @param m map that contains every possible strings with fitness
     * @param iteration the number of iteration (if iteration = -1 we are printing the initial population 
     */
    public static void writeOnCsvFile(String[] population, Map m, int iteration) {
        try {
            for (int i = 0; i < population.length; i++) {
                bw.write(iteration+";"+ population[i] + ";" + m.get(population[i]));
                bw.newLine();
            }
        } catch (IOException ex) {
        }
    }

}
